#
# ~/.bashrc
#

[[ $- != *i* ]] && return

colors() {
	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}

function rmtmp() {

	rm tmp/*

}

function rmcache() {

	rm .cache/* -r

}

function orphan() {

	sudo pacman -Rns $(sudo pacman -Qtdq)

}

function tbc() {

	sudo mount /dev/sdc1 /mark

}

function dm() {

	vim .xinitrc && startx

}

function rainbowsys() {

	rm rb.txt && neofetch >> rb.txt && clear && cat rb.txt | lolcat

}

function world() {

	telnet mapscii.me

}

function feed() {

	canto-curses

}

function mpv-net() {

	clear
	echo "Enter Video URL: "
	read x
	mpv $x & disown -a && exit

}

function neton() {

	sudo wifi-menu -o

}

function netoff() {

	sudo killall wpa_supplicant

}

function i3-wall() {

	Documents/Coding/Bash/i3wallpaper.sh

}

function confsh() {

vim .bashrc

}

function weather() {

w3m wttr.in/sheboygan

}

function neo() {

clear && neofetch

}

function duckit() {

w3m duckduckgo.com

}

function cava() {

	sh Downloads/cava/cava

}

function tbm() {

sudo mount /dev/sdb1 /mark

}

function tbu() {

sudo umount -R /mark

}

function confi3() {

vim /home/mark/.config/i3/config

}

[[ -f ~/.extend.bashrc ]] && . ~/.extend.bashrc

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

[ -n "$XTERM_VERSION" ] && transset-df --id "$WINDOWID" >/dev/null -m 0.9
